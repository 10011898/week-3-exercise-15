﻿using System;

namespace week_3_exercise_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number to me multiplied.");
            var multNum = double.Parse(Console.ReadLine());
            for(var i=1; i<=12; i++)
            {
                Console.WriteLine($"{multNum} x {i} = {multNum*i}");
            }
        }
    }
}
